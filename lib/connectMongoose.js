'use strict';

const mongoose = require('mongoose');
const db = mongoose.connection;

var databaseUri = process.env.DATABASE_URI;

if (!databaseUri) {
    console.log('DATABASE_URI not specified, falling back to localhost.');    
}

mongoose.Promise = global.Promise;

db.on('error', function (err) {
  console.error('mongodb connection error:', err);
  process.exit(1);
});

db.once('open', function () {
  console.info('Connected to mongodb.');
});

mongoose.connect(databaseUri);

module.exports = db;
